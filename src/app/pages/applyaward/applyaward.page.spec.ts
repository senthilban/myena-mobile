import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplyawardPage } from './applyaward.page';

describe('ApplyawardPage', () => {
  let component: ApplyawardPage;
  let fixture: ComponentFixture<ApplyawardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplyawardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplyawardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
