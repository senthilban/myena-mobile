import { Component, OnInit } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, NavigationExtras } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { StorageService, Item } from '../../services/storage.service';
import { SharedService } from '../../services/shared.service';
import * as moment from 'moment';
import * as _ from 'lodash';
import { environment } from '../../../environments/environment';

const URL = environment.api + '/api/';

@Component({
  selector: 'app-applyaward',
  templateUrl: './applyaward.page.html',
  styleUrls: ['./applyaward.page.scss'],
})
export class ApplyawardPage implements OnInit {

  applyawardItems:any = [];
  businessUser:any = [];
  business:any = [];
  award:any = [];
  showApplyAward = true;
  userId;

  constructor(private router: Router,public formBuilder: FormBuilder, 
    private http: HttpClient, 
    public route : ActivatedRoute, 
    private storage: Storage, 
    private storageService: StorageService,
    public sharedService: SharedService) { }

    ngOnInit() {
      this.showApplyAward = true;
      this.userId = this.sharedService.getUserId();
      this.getBusinessUser();
    }

    doRefresh(event){
      this.ngOnInit();
      event.target.complete();
    }

    getBusinessUser(){
      let userId = this.sharedService.getUserId();
      let url = URL + 'myusers?user_id=' + userId;
      this.http.get(url).subscribe(res => {
        if(res['data'].length > 0){
          this.businessUser = res['data'];
        }
        this.getBusiness();
      });
    }

    getBusiness(){
      let url = URL + 'business?user_id=' + this.userId;
      this.http.get(url).subscribe(res => {
        if(res['data'].length > 0){
          this.business = res['data'];
        }
        this.getAward();
      });
    }

    getAward(){
      let url = URL + 'award';
      this.http.get(url).subscribe(res => {
        if(res['data'].length > 0){
          this.award = res['data'];
        }
        this.getBoaward();
      });
    }

    getBoaward(){
      let url = URL + 'boaward?user_id=' + this.userId;
      this.http.get(url).subscribe(res => {
        if(res['data'].length > 0){
          this.applyawardItems = res['data'];
        }
        this.modifiedItems(this.applyawardItems);
        this.showApplyAward = false;
      });
    }

    modifiedItems(info){
      info.forEach(item => {
        let bo_id_name = _.find(this.businessUser, (data) => data.user_id == item.bo_id).name;
        item['name'] = bo_id_name;
      });
      this.applyawardItems = info;
    }

    addBoawardItem(){
      this.sharedService.setAddStatus(true);
      this.router.navigate(['/menu/applyawardform']);
    }

    viewBoawardItem(item){
      let bo_id_name = _.find(this.businessUser, (data) => data.id == item.bo_id).name;
      item.bo_id = bo_id_name;
      let bus_id_name = _.find(this.business, (data) => data.id == item.bus_id).name;
      item.bus_id = bus_id_name;
      let awd_id_name = _.find(this.award, (data) => data.id == item.awd_id).name;
      item.awd_id = awd_id_name;
      this.sharedService.setBOAward(item);
      this.router.navigate(['/menu/applyawarddetail']);
    }

    editABoawardItem(item){
      this.sharedService.setAddStatus(false);
      this.sharedService.setBOAward(item);
      this.router.navigate(['/menu/applyawardform']);
    }

    removeBoawardItem(item){
      let url = URL + 'boaward?id=' + item.id;
      this.http.delete(url).subscribe(res => {
        alert('Award successfully deleted');
        this.ngOnInit();
      });
    }

}
