import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../services/shared.service';

@Component({
  selector: 'app-applyawarddetail',
  templateUrl: './applyawarddetail.page.html',
  styleUrls: ['./applyawarddetail.page.scss'],
})
export class ApplyawarddetailPage implements OnInit {
  public boawards:any = {};

  constructor(public sharedService: SharedService) { }

  ngOnInit() {
    this.boawards = this.sharedService.getBOAward();
  }

}
