import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { StorageService, Item } from '../../services/storage.service';
import * as moment from 'moment';
import { SharedService } from '../../services/shared.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

const URL = environment.api + '/api/';

@Component({
  selector: 'app-applyawardform',
  templateUrl: './applyawardform.page.html',
  styleUrls: ['./applyawardform.page.scss'],
})
export class ApplyawardformPage implements OnInit {
  applyAwardForm: FormGroup;
  items: Item[] = [];
 
  newItem: Item = <Item>{};

  data: any = {
    boid: '',
    busid: '',
    awdid: ''
  };

  public award:any = {};

  businessUser:any = [];
  business:any = [];
  awards:any = [];
  userId;

  selectedBusinessUser:string = '';
  selectedBusinessId: string = '';
  selectedAwardId: string = '';

  status;
  constructor(private router: Router, public formBuilder: FormBuilder, 
    private http: HttpClient, 
    public route : ActivatedRoute, 
    private storage: Storage, 
    private storageService: StorageService,
    public sharedService: SharedService) {
      this.applyAwardForm = formBuilder.group({
        boid: [''],
        busid: [''],
        awdid: [''],
        actamount: [''],
        paidamount: [''],
        winner: [''],
        datewin: [''],
        is_used: 0
      });
    }

  ngOnInit() {
    this.userId = this.sharedService.getUserId();
    this.getBusinessUser();
    this.getBusiness();
    this.getAward();
    this.status = this.sharedService.getAddStatus();
    if(!this.status){
      this.award = this.sharedService.getBOAward();
      this.data.boid = this.award['bo_id'];
      this.data.busid = this.award['bus_id'];
      this.data.awdid = this.award['awd_id'];
      this.applyAwardForm.controls['boid'].setValue(this.award['bo_id']);
      this.applyAwardForm.controls['busid'].setValue(this.award['bus_id']);
      this.applyAwardForm.controls['awdid'].setValue(this.award['awd_id']);
      this.applyAwardForm.controls['actamount'].setValue(this.award['act_amount']);
      this.applyAwardForm.controls['paidamount'].setValue(this.award['paid_amount']);
      this.applyAwardForm.controls['winner'].setValue(this.award['winner']);
      this.applyAwardForm.controls['datewin'].setValue(this.award['date_win']);
    }
  }

  selected(name){
    this.selectedBusinessUser = name;
  }

  selectedBusiness(id){
    this.selectedBusinessId = id;
  }

  selectedAward(id){
    this.selectedAwardId = id;
  }

  getBusinessUser(){
    let userId = this.sharedService.getUserId();
    let url = URL + 'myusers?user_id=' + userId;
    this.http.get(url).subscribe(res => {
      if(res['data'].length > 0){
        this.businessUser = res['data'];
      }
    });
  }

  getBusiness(){
    let url = URL + 'business?user_id=' + this.userId;
    this.http.get(url).subscribe(res => {
      if(res['data'].length > 0){
        this.business = res['data'];
      }
    });
  }

  getAward(){
    let url = URL + 'award';
    this.http.get(url).subscribe(res => {
      if(res['data'].length > 0){
        this.awards = res['data'];
      }
    });
  }

  onSubmit(value){
    value['datewin'] = '2019-06-10';

    let id;
    let reqData = {
      bo_id: value.boid,
      bus_id: value.busid,
      awd_id: value.awdid,
      act_amount: value.actamount,
      date_win: moment(value['datewin']).format("YYYY-MM-DD HH:mm:ss"),
      paid_amount: value.paidamount,
      winner: value.winner,
      is_used: 0,
      user_id: this.userId
    };
    if(!this.status){
      let boaward_data = this.sharedService.getBOAward();
      id = boaward_data.id;
      reqData['id'] = id;
    }

    
    
    let url = URL + 'boaward';
    if(this.status){
      this.http.post(url, reqData).subscribe(res => {
        this.applyAwardForm.reset();
        this.router.navigate(['/menu/applyaward']);
        alert('Award successfully created');
      });
    } else {
      this.http.put(url, reqData).subscribe(res => {
        this.applyAwardForm.reset();
        this.router.navigate(['/menu/applyaward']);
        alert('Award successfully updated');
      });
    }
    
  }

}
