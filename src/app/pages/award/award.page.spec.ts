import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AwardPage } from './award.page';

describe('AwardPage', () => {
  let component: AwardPage;
  let fixture: ComponentFixture<AwardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AwardPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
