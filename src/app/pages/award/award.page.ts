import { Component, OnInit } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, NavigationExtras } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { StorageService, Item } from '../../services/storage.service';
import { SharedService } from '../../services/shared.service';
import * as moment from 'moment';
import { environment } from '../../../environments/environment';

const URL = environment.api;

@Component({
  selector: 'app-award',
  templateUrl: './award.page.html',
  styleUrls: ['./award.page.scss'],
})
export class AwardPage implements OnInit {

  awardItems:any = [];
  showAward = true;

  constructor(private router: Router,public formBuilder: FormBuilder, 
    private http: HttpClient, 
    public route : ActivatedRoute, 
    private storage: Storage, 
    private storageService: StorageService,
    public sharedService: SharedService) { }

  ngOnInit() {
    this.showAward = true;
    let url = URL + '/api/award';
    this.http.get(url).subscribe(res => {
      if(res['data'].length > 0){
        this.awardItems = res['data'];
      }
      this.showAward = false;
    });
  }

  addAwardItem(){
    this.sharedService.setAddStatus(true);
    this.router.navigate(['/menu/awardform']);
  }

  viewAwardItem(item){
    this.sharedService.setAward(item);
    this.router.navigate(['/menu/awarddetails']);
  }

  editAwardItem(item){
    this.sharedService.setAddStatus(false);
    this.sharedService.setAward(item);
    this.router.navigate(['/menu/awardform']);
  }

  removeAwaredItem(item){
    let url = URL + '/api/award?id=' + item.id;
    this.http.delete(url).subscribe(res => {
      alert('Award successfully deleted');
      this.ngOnInit();
    });
  }

  doRefresh(event){
    this.ngOnInit();
    event.target.complete();
  }

}
