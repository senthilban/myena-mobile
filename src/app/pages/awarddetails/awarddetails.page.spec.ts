import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AwarddetailsPage } from './awarddetails.page';

describe('AwarddetailsPage', () => {
  let component: AwarddetailsPage;
  let fixture: ComponentFixture<AwarddetailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AwarddetailsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwarddetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
