import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../services/shared.service';

@Component({
  selector: 'app-awarddetails',
  templateUrl: './awarddetails.page.html',
  styleUrls: ['./awarddetails.page.scss'],
})
export class AwarddetailsPage implements OnInit {
  public awards:any = {};

  constructor(public sharedService: SharedService) { }

  ngOnInit() {
    this.awards = this.sharedService.getAward();
  }

}
