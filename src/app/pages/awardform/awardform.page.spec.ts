import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AwardformPage } from './awardform.page';

describe('AwardformPage', () => {
  let component: AwardformPage;
  let fixture: ComponentFixture<AwardformPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AwardformPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwardformPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
