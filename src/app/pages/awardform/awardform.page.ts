import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { StorageService, Item } from '../../services/storage.service';
import * as moment from 'moment';
import { SharedService } from '../../services/shared.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

const URL = environment.api;

@Component({
  selector: 'app-awardform',
  templateUrl: './awardform.page.html',
  styleUrls: ['./awardform.page.scss'],
})
export class AwardformPage implements OnInit {
  awardForm: FormGroup;
  items: Item[] = [];
 
  newItem: Item = <Item>{};

  public award:any = {};
  status;
  constructor(private router: Router, public formBuilder: FormBuilder, 
    private http: HttpClient, 
    public route : ActivatedRoute, 
    private storage: Storage, 
    private storageService: StorageService,
    public sharedService: SharedService) {
      this.awardForm = formBuilder.group({
        name: [''],
        amount: [''],
        desc: [''],
        baid: [''],
        is_used: 0
      });
    }

  ngOnInit() {
    this.status = this.sharedService.getAddStatus();
    if(!this.status){
      this.award = this.sharedService.getAward();
      this.awardForm.controls['name'].setValue(this.award['name']);
      this.awardForm.controls['amount'].setValue(this.award['amount']);
      this.awardForm.controls['desc'].setValue(this.award['desc']);
      this.awardForm.controls['baid'].setValue(this.award['baid']);
    }
  }

  onSubmit(value){
    let id;
    let reqData = {
      name: value.name,
      amount: value.amount,
      desc: value.desc,
      baid: value.baid,
      is_used: 0
    };
    if(!this.status){
      let award_data = this.sharedService.getAward();
      id = award_data.id;
      reqData['id'] = id;
    }
    
    console.log(reqData);
    let url = URL + '/api/award';
    if(this.status){
      this.http.post(url, reqData).subscribe(res => {
        this.awardForm.reset();
        this.router.navigate(['/menu/award']);
        alert('Award successfully created');
      });
    } else {
      this.http.put(url, reqData).subscribe(res => {
        this.awardForm.reset();
        this.router.navigate(['/menu/award']);
        alert('Business successfully updated');
      });
    }
    
  }

}
