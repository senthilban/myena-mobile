import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessformPage } from './businessform.page';

describe('BusinessformPage', () => {
  let component: BusinessformPage;
  let fixture: ComponentFixture<BusinessformPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessformPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessformPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
