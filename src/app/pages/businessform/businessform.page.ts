import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { StorageService, Item } from '../../services/storage.service';
import * as moment from 'moment';
import { SharedService } from '../../services/shared.service';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

const URL = environment.api;

@Component({
  selector: 'app-businessform',
  templateUrl: './businessform.page.html',
  styleUrls: ['./businessform.page.scss'],
})
export class BusinessformPage implements OnInit {
  businessForm: FormGroup;
  items: Item[] = [];
  toast:any;
  userId;

  data: any = {
    type: '',
    nopartners: '',
    bscid: '',
    bstcid: ''
  };
 
  newItem: Item = <Item>{};

  public business:any = {};
  businessSubCategory:any = [];
  businessSubTwoCategory:any = [];
  status;

  constructor(public formBuilder: FormBuilder, 
    private http: HttpClient, 
    public route : ActivatedRoute, 
    private storage: Storage, 
    private storageService: StorageService,
    public sharedService: SharedService,
    public toastController: ToastController,
    private router: Router) {
      this.businessForm = formBuilder.group({
        name: [''],
        email: [''],
        mobile: [''],
        landline: [''],
        est: [''],
        address: [''],
        type: [''],
        nopartners: [''],
        gst: [''],
        regno: [''],
        bsc: [''],
        bstc: [''],
        is_used: 0
      });
    }

  ngOnInit() {
    this.getBusinessSubCategoryData();
    // this.getBusinessSubTwoCategoryData();
    this.userId = this.sharedService.getUserId();
    this.status = this.sharedService.getAddStatus();
    if(!this.status){
      this.business = this.sharedService.getBusiness();
      this.data.type = this.business['type'];
      this.data.nopartners = this.business['nopartners'];
      this.businessForm.controls['name'].setValue(this.business['name']);
      this.businessForm.controls['email'].setValue(this.business['email']);
      this.businessForm.controls['mobile'].setValue(this.business['mobile']);
      this.businessForm.controls['landline'].setValue(this.business['landline']);
      this.businessForm.controls['est'].setValue(this.business['est']);
      this.businessForm.controls['address'].setValue(this.business['address']);
      this.businessForm.controls['type'].setValue(this.business['type']);
      this.businessForm.controls['nopartners'].setValue(this.business['nopartners']);
      this.businessForm.controls['gst'].setValue(this.business['gst']);
      this.businessForm.controls['regno'].setValue(this.business['regno']);
    }
  }

  getBusinessSubCategoryData(){
    let url = URL + '/api/business_sub_category';
    this.http.get(url).subscribe(res => {
      if(res['data'].length > 0){
        this.businessSubCategory = res['data'];
      }
    });
  }

  businessSubChange(event){
    let url = URL + '/api/business_sub_two_category?bscid=' + this.data.bscid;
    this.http.get(url).subscribe(res => {
      if(res['data'].length > 0){
        this.businessSubTwoCategory = res['data'];
      }
    });
  }

  onSubmit(value){
    let id;
    let reqData = {
      name: value.name,
      email: value.email,
      mobile: value.mobile,
      landline: value.landline,
      yearofest: moment(value['est']).format("YYYY-MM-DD HH:mm:ss"),
      address: value.address,
      type: value.type,
      nopartners: value.nopartners,
      gst: value.gst,
      regno: value.regno,
      is_used: 0,
      user_id: this.userId,
      bsc_id: this.data.bscid,
      bsct_id: this.data.bstcid
    };
    if(!this.status){
      let business_data = this.sharedService.getBusiness();
      id = business_data.id;
      reqData['id'] = id;
    }

    
    console.log(reqData);
    let url = URL + '/api/business';
    if(this.status){
      this.http.post(url, reqData).subscribe(res => {
        this.businessForm.reset();
        this.router.navigate(['/menu/first']);
        
        alert('Business successfully created');
      });
    } else {
      this.http.put(url, reqData).subscribe(res => {
        this.businessForm.reset();
        this.router.navigate(['/menu/first']);
        alert('Business successfully updated');
      });
    }
    
  }

  showToast(message) {
    this.toast = this.toastController.create({
      message: message,
      duration: 2000
    }).then((toastData)=>{
      toastData.present();
    });
  }
  HideToast(){
    this.toast = this.toastController.dismiss();
  }

}