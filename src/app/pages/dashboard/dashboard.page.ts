import { Component, OnInit } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, NavigationExtras } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { StorageService, Item } from '../../services/storage.service';
import { SharedService } from '../../services/shared.service';
import * as moment from 'moment';
import * as _ from 'lodash';
import { environment } from '../../../environments/environment';

const URL = environment.api + '/api/';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  applyawardItems:any = [];
  businessUser:any = [];
  business:any = [];
  award:any = [];
  showDashboard = true;
  userId;
  userObj;

  constructor(private router: Router,public formBuilder: FormBuilder, 
    private http: HttpClient, 
    public route : ActivatedRoute, 
    private storage: Storage, 
    private storageService: StorageService,
    public sharedService: SharedService) { }

  ngOnInit() {
    this.showDashboard = true;
    console.log(this.sharedService.getSocial());

    this.sharedService.setUserName('senthil', 'nallathambi');
    this.getUserId();
    this.getBusinessUser();
  }

  getUserId(){
    let items = this.sharedService.getSocial();
    let main_url = URL + 'user';
    let uri = main_url + '?user_id=' + items['user_id'];
    this.http.get(uri).subscribe(res => {
      this.userObj = res['data'][0];
      this.userId = res['data'][0]['id'];
      this.sharedService.setUserId(this.userId);
      this.sharedService.setPrimaryBusiness(this.userObj['primary_business']);
      this.getBusinessUser();
    });
  }

  getBusinessUser(){
    let userId = this.sharedService.getUserId();
    let url = URL + 'myusers?user_id=' + userId;
    this.http.get(url).subscribe(res => {
      if(res['data'].length > 0){
        this.businessUser = res['data'];
        this.sharedService.setMyUsers(res['data']);
      }
      this.getBusiness();
    });
  }

  getBusiness(){
    let userId = this.sharedService.getUserId();
    let url = URL + 'business?user_id=' + this.userId;
    this.http.get(url).subscribe(res => {
      if(res['data'].length > 0){
        this.business = res['data'];
      }
      this.getAward();
    });
  }

  getAward(){
    let url = URL + 'award';
    this.http.get(url).subscribe(res => {
      if(res['data'].length > 0){
        this.award = res['data'];
      }
      this.getBoaward();
    });
  }

  getBoaward(){
    let url = URL + 'boaward';
    this.http.get(url).subscribe(res => {
      if(res['data'].length > 0){
        this.applyawardItems = res['data'];
      }
      this.showDashboard = false;
    });
  }

}
