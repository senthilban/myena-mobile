import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../services/shared.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  public business:any = {};

  constructor(public sharedService: SharedService) { }

  ngOnInit() {
    this.business = this.sharedService.getBusiness();
  }

}
