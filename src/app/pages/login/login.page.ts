import { Component, OnInit } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, NavigationExtras } from '@angular/router';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { StorageService, Item } from '../../services/storage.service';
import * as moment from 'moment';
import { SharedService } from '../../services/shared.service';
import { environment } from '../../../environments/environment';

const URL = environment.api;

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  items: Item[] = [];
 
  newItem: Item = <Item>{};
  user = {};
  fbData:any = {};
  googleData:any = {};
  showSpinner = false;

  constructor(private router: Router, 
    private fb: Facebook, 
    private googlePlus: GooglePlus, 
    private http:HttpClient,
    private storage: Storage,
    private storageService: StorageService,
    public sharedService: SharedService) { }

  ngOnInit() {
    // this.router.navigate(['/menu/dashboard']);
  }

  beforeLogin(){
    this.fb.getLoginStatus().then((response) => {
      if (response.status == 'connected') {
        this.fb.logout()
        .then(res => {
          this.loginFb();
        }, err => {
          console.log(err);
        });
      } else {
        this.loginFb();
      }
    })
  }

  loginFb(){
      this.fb.login(['public_profile', 'email'])
        .then((res: FacebookLoginResponse) => {
          if(res.status === 'connected'){
            this.getFbData(res.authResponse.accessToken);
            this.user['img'] = 'https://graph.facebook.com/' + res.authResponse.userID + '/picture?type=square';
            // this.fbData.image = this.user['img'];
          } else {
            alert('login failed');
          }
        })
        .catch(e => console.log('Error logging into Facebook', e));
    
  }

  getFbData(access_token:string){
    let url = 'https://graph.facebook.com/me?fields=id,name,first_name,last_name,email&access_token=' + access_token; 
    this.http.get(url).subscribe(data => {
      this.fbData = data;
      this.fbData.image = this.user['img'];
      this.checkUserExist(this.fbData, 'fb');
    });
  }

  loginGoogle(){
    this.showSpinner = true;
    this.googlePlus.login({})
    .then(res => {
      this.googleData = res;
      this.getGoogleData();
    })
    .catch(err => console.error(err));
  }

  getGoogleData(){
    let token = this.googleData.accessToken;
    let url = 'https://www.googleapis.com/plus/v1/people/me?access_token=' + token;
    this.http.get(url).subscribe(data => {
      this.googleData.name = data['displayName'];
      this.googleData.image = data['image']['url'];
      this.checkUserExist(this.googleData, 'google');
    });
  }

  checkUserExist(data, name){
    let user_id;
    let user_name;
    let firstname;
    let lastname;
    let email;
    let url;

    if(name === 'fb'){
      user_id = data.id;
      user_name = data.name;
      firstname = data.first_name;
      lastname = data.last_name;
      email = data.email;
      url = data.image;
      this.sharedService.setAppName('fb');
    } else {
      user_id = data.userId;
      user_name = data.displayName;
      firstname = data.givenName;
      lastname = data.familyName;
      email = data.email;
      url = data.image;
      this.sharedService.setAppName('google');
    }

    let main_url = URL + '/api/user';
    let uri = main_url + '?user_id=' + user_id;
    this.http.get(uri).subscribe(first_res => {
      if(first_res['status'] === 'Failure'){
        let req_data = {
          "user_id": user_id,
          "name": user_name,
          "firstName": firstname,
          "lastName": lastname,
          "email": email
        };
        this.http.post(main_url, req_data).subscribe(res => {
          this.newItem = <Item>{};
          this.newItem.user_id = user_id;
          this.newItem.user_name = user_name;
          this.newItem.firstname = firstname;
          this.newItem.lastname = lastname;
          this.newItem.email = email;
          this.newItem.url = url;
          this.sharedService.setSocial(this.newItem);
          this.showSpinner = false;
          this.router.navigate(['/menu/second']);
        });
      } else {
        if(first_res['data'].length == 1) {
          this.newItem = <Item>{};
          this.newItem.user_id = user_id;
          this.newItem.user_name = user_name;
          this.newItem.firstname = firstname;
          this.newItem.lastname = lastname;
          this.newItem.email = email;
          this.newItem.url = url;
          this.sharedService.setSocial(this.newItem);
          this.showSpinner = false;
          this.router.navigate(['/menu/dashboard']);
        }
      }
    });
  }
}
