import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [
      {
        path: 'first',
        loadChildren: '../first-with-tabs/first-with-tabs.module#FirstWithTabsPageModule'
      },
      {
        path: 'second',
        loadChildren: '../second/second.module#SecondPageModule'
      },
      {
        path: 'second/details',
        loadChildren: '../details/details.module#DetailsPageModule'
      },
      {
        path: 'dashboard', 
        loadChildren: '../dashboard/dashboard.module#DashboardPageModule' 
      },
      { 
        path: 'users', 
        loadChildren: '../users/users.module#UsersPageModule' 
      },
      { 
        path: 'usersforms', 
        loadChildren: '../usersforms/usersforms.module#UsersformsPageModule' 
      },
      { path: 'businessform', 
        loadChildren: '../businessform/businessform.module#BusinessformPageModule' 
      },
      { 
        path: 'award', 
        loadChildren: '../award/award.module#AwardPageModule' 
      },
      { 
        path: 'applyaward', 
        loadChildren: '../applyaward/applyaward.module#ApplyawardPageModule' 
      },
      { 
        path: 'awardform', 
        loadChildren: '../awardform/awardform.module#AwardformPageModule' 
      },
      { 
        path: 'awarddetails', 
        loadChildren: '../awarddetails/awarddetails.module#AwarddetailsPageModule' 
      },
      { 
        path: 'applyawardform', 
        loadChildren: '../applyawardform/applyawardform.module#ApplyawardformPageModule' 
      },
      { 
        path: 'applyawarddetail', 
        loadChildren: '../applyawarddetail/applyawarddetail.module#ApplyawarddetailPageModule' 
      },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
