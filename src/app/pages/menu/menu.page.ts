import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { StorageService, Item } from '../../services/storage.service';
import * as moment from 'moment';
import { SharedService } from '../../services/shared.service';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
 
@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  selectedPath = '';
  items1: Item[] = [];
  userAvatar = '';
  userName = '';
 
  newItem: Item = <Item>{};
 
  pages = [
    {
      title: 'My Dashboard',
      url: '/menu/dashboard'
    },
    {
      title: 'My Profile',
      url: '/menu/second'
    },
    {
      title: 'My Users',
      url: '/menu/users'
    },
    {
      title: 'My Business',
      url: '/menu/first'
    },
    {
      title: 'Apply Awards',
      url: '/menu/applyaward'
    }
  ];


 
  constructor(private router: Router, 
    private storageService: StorageService, 
    public sharedService: SharedService,
    private googlePlus: GooglePlus,
    private fb: Facebook) {
    this.router.events.subscribe((event: RouterEvent) => {
      if (event && event.url) {
        this.selectedPath = event.url;
      }
    });
  }

  loadItems() {
    let items = this.sharedService.getSocial();
    this.userAvatar = items['url'];
    this.userName =  items['user_name'];
  }
 
  ngOnInit() {
    this.loadItems();
  }

  doLogout(){
    let appName = this.sharedService.getAppName();
    if(appName === 'fb'){
      this.fb.logout()
      .then(res => {
        console.log('doLogout fb inside');
        //user logged out so we will remove him from the NativeStorage
        console.log(res);
        this.router.navigate(['/login']);
      }, err => {
        console.log(err);
      });
    } else {
      this.googlePlus.logout().then(res => {
        console.log('doLogout google inside');
        //user logged out so we will remove him from the NativeStorage
        console.log(res);
        this.router.navigate(['/login']);
      }, err => {
        console.log(err);
      });
    }
    console.log('doLogout');
    
  }
 
}
