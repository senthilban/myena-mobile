import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { StorageService, Item } from '../../services/storage.service';
import * as moment from 'moment';
import { environment } from '../../../environments/environment';
import { SharedService } from '../../services/shared.service';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, NavigationExtras } from '@angular/router';

const URL = environment.api;

@Component({
  selector: 'app-second',
  templateUrl: './second.page.html',
  styleUrls: ['./second.page.scss'],
})
export class SecondPage implements OnInit {
  items: Item[] = [];
  userId;
  business:any = [];
 
  newItem: Item = <Item>{};
  data:any = {
    primary_business: ''
  };

  userObj:any;

  selectedBusinessUser:string = '';
  selectedBusinessId: string = '';

  authForm: FormGroup;
  // data: any;
  heroes$: Observable<any>;

  constructor(public formBuilder: FormBuilder, 
    private http: HttpClient, private router: Router,
    public route : ActivatedRoute, private storage: Storage, private storageService: StorageService, public sharedService: SharedService) { 
    this.authForm = formBuilder.group({
        name: [''],
        fname: [''],
        lname: [''],
        email: [''],
        mobile: [''],
        primary_business: [''],
        dob: [''],
        address: ['']
    });
  }

  loadItems() {
    this.items = this.sharedService.getSocial();
    this.authForm.controls['name'].setValue(this.items['user_name']);
    this.authForm.controls['fname'].setValue(this.items['firstname']);
    this.authForm.controls['lname'].setValue(this.items['lastname']);
    this.authForm.controls['email'].setValue(this.items['email']);
    this.authForm.controls['mobile'].setValue(this.userObj['mobile']);
    this.authForm.controls['dob'].setValue(this.userObj['dob']);
    this.authForm.controls['address'].setValue(this.userObj['address']);
    this.authForm.controls['primary_business'].setValue(this.userObj['primary_business']);
    this.data['primary_business'] = this.userObj['primary_business'];
    this.sharedService.setPrimaryBusiness(this.userObj['primary_business']);
  }

  getUserId(){
    let items = this.sharedService.getSocial();
    let main_url = URL + '/api/user';
    let uri = main_url + '?user_id=' + items['user_id'];
    this.http.get(uri).subscribe(res => {
      this.userObj = res['data'][0];
      this.userId = res['data'][0]['id'];
      this.sharedService.setUserId(this.userId);
      this.getBusiness();
    });
  }

  

  ngOnInit() {
    this.getUserId();
  }

  selectedBusiness(id){
    this.selectedBusinessId = id;
  }

  getBusiness(){
    let url = URL + '/api/business?user_id=' + this.userId;
    this.http.get(url).subscribe(res => {
      if(res['data'].length > 0){
        this.business = res['data'];
        this.loadItems();
      } else {
        alert('Please add business');
        this.router.navigate(['/menu/dashboard']);
      }
    });
  }

  

  onSubmit(value: any){
    let main_url = URL + '/api/user';
    let dob = moment(value['dob']).format("YYYY-MM-DD HH:mm:ss");
    let req_data = {
      "user_id": this.userId,
      "primary_business": value.primary_business,
      "dob": dob,
      "address": value.address,
      "mobile": value.mobile
    }
    this.http.put(main_url, req_data).subscribe(res => {
      console.log(res);
      console.log('update success');
      alert('successfully updated');
    });
  }

}
