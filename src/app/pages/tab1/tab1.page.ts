import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { StorageService, Item } from '../../services/storage.service';
import { SharedService } from '../../services/shared.service';
import * as moment from 'moment';
import { environment } from '../../../environments/environment';
import * as _ from 'lodash';

const URL = environment.api;

@Component({
  selector: 'app-tab1',
  templateUrl: './tab1.page.html',
  styleUrls: ['./tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  items = [];
  showTab = true;
  userId;
  primary_business;

  constructor(private router: Router,public formBuilder: FormBuilder, 
    private http: HttpClient, 
    public route : ActivatedRoute, 
    private storage: Storage, 
    private storageService: StorageService,
    public sharedService: SharedService) { }

  ngOnInit() {
    this.showTab = true;
    let items = this.sharedService.getSocial();
    this.userId = this.sharedService.getUserId();
    let primary_id = this.sharedService.getPrimaryBusiness();
    let url = URL + '/api/business?user_id=' + this.userId;
    this.http.get(url).subscribe(res => {
      if(res['data'].length > 0){
        this.items = res['data'];
        this.getUserId();
      }
      this.showTab = false;
    });
  }

  getUserId(){
    let main_url = URL + '/api/bu_owners';
    let uri = main_url + '?user_id=' + this.userId;
    this.http.get(uri).subscribe(res => {
      let pb = res['data'][0]['primary_business'];
      this.primary_business = _.find(this.items, (data) => data.id == pb).name;
      this.sharedService.setPrimaryBusiness(this.primary_business);
    });
  }

  removeItem(item){
    let url = URL + '/api/business?id=' + item.id;
    this.http.delete(url).subscribe(res => {
      alert('Business successfully deleted');
    });
  }

  viewItem(item){
    this.sharedService.setBusiness(item);
    this.router.navigate(['/menu/first/tabs/business/details']);
  }

  editItem(item){
    this.sharedService.setAddStatus(false);
    this.sharedService.setBusiness(item);
    this.router.navigate(['/menu/businessform']);
  }

  addItem(){
    this.sharedService.setAddStatus(true);
    this.router.navigate(['/menu/businessform']);
  }

  doRefresh(event){
    this.ngOnInit();
    event.target.complete();
  }
}
