import { Component, OnInit } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, NavigationExtras } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { StorageService, Item } from '../../services/storage.service';
import { SharedService } from '../../services/shared.service';
import * as moment from 'moment';
import * as _ from 'lodash';
import { environment } from '../../../environments/environment';

const URL = environment.api + '/api/';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
  items = [];
  showTab = true;
  userId;
  primary_business;
  businessUser:any = [];

  constructor(private router: Router,public formBuilder: FormBuilder, 
    private http: HttpClient, 
    public route : ActivatedRoute, 
    private storage: Storage, 
    private storageService: StorageService,
    public sharedService: SharedService) { }

  ngOnInit() {
    this.showTab = false;
    let userId = this.sharedService.getUserId();
    let url = URL + 'myusers?user_id=' + userId;
    this.http.get(url).subscribe(res => {
      if(res['data'].length > 0){
        this.businessUser = res['data'];
      }
      this.showTab = false;
    });
  }

  removeItem(item){
    let url = URL + 'myusers?id=' + item.id;
    this.http.delete(url).subscribe(res => {
      alert('Business user successfully deleted');
    });
  }

  addItem(){
    this.sharedService.setAddStatus(true);
    this.router.navigate(['/menu/usersforms']);
  }

  doRefresh(event){
    this.ngOnInit();
    event.target.complete();
  }
}
