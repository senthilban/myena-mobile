import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersformsPage } from './usersforms.page';

describe('UsersformsPage', () => {
  let component: UsersformsPage;
  let fixture: ComponentFixture<UsersformsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersformsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersformsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
