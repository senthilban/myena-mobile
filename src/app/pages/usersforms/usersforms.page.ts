import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { StorageService, Item } from '../../services/storage.service';
import * as moment from 'moment';
import { environment } from '../../../environments/environment';
import { SharedService } from '../../services/shared.service';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, NavigationExtras } from '@angular/router';

const URL = environment.api;

@Component({
  selector: 'app-usersforms',
  templateUrl: './usersforms.page.html',
  styleUrls: ['./usersforms.page.scss'],
})
export class UsersformsPage implements OnInit {
  items: Item[] = [];
  userId;
  business:any = [];
 
  newItem: Item = <Item>{};
  data:any = {
    primary_business: ''
  };

  userObj:any;

  selectedBusinessUser:string = '';
  selectedBusinessId: string = '';

  authForm: FormGroup;
  // data: any;
  heroes$: Observable<any>;

  constructor(public formBuilder: FormBuilder, 
    private http: HttpClient, private router: Router,
    public route : ActivatedRoute, private storage: Storage, private storageService: StorageService, public sharedService: SharedService) { 
    this.authForm = formBuilder.group({
        name: [''],
        fname: [''],
        lname: [''],
        email: [''],
        mobile: [''],
        primary_business: [''],
        dob: [''],
        employee: ['false'],
        address: ['']
    });
  }

  loadItems() {
    this.items = this.sharedService.getSocial();
    this.authForm.controls['name'].setValue(this.items['user_name']);
    this.authForm.controls['fname'].setValue(this.items['firstname']);
    this.authForm.controls['lname'].setValue(this.items['lastname']);
    this.authForm.controls['email'].setValue(this.items['email']);
    this.authForm.controls['mobile'].setValue(this.userObj['mobile']);
    this.authForm.controls['dob'].setValue(this.userObj['dob']);
    this.authForm.controls['address'].setValue(this.userObj['address']);
    this.authForm.controls['primary_business'].setValue(this.userObj['primary_business']);
    this.data['primary_business'] = this.userObj['primary_business'];
    this.sharedService.setPrimaryBusiness(this.userObj['primary_business']);
  }

  getUserId(){
    let items = this.sharedService.getSocial();
    let main_url = URL + '/api/user';
    let uri = main_url + '?user_id=' + items['user_id'];
    this.http.get(uri).subscribe(res => {
      this.userObj = res['data'][0];
      this.userId = res['data'][0]['id'];
      this.sharedService.setUserId(this.userId);
      this.getBusiness();
    });
  }

  

  ngOnInit() {
    this.getUserId();
  }

  selectedBusiness(id){
    this.selectedBusinessId = id;
  }

  getBusiness(){
    let url = URL + '/api/business?user_id=' + this.userId;
    this.http.get(url).subscribe(res => {
      if(res['data'].length > 0){
        this.getBusinessSubCategory(res['data']);
      } else {
        alert('Please add business');
        this.router.navigate(['/menu/dashboard']);
      }
    });
  }

  getBusinessSubCategory(data){
    let callData:any = [];
    let url = URL + '/api/user_bsc?bscid=';
    data.forEach(item => {
      let mainurl = this.http.get(url + item.bsc_id);
      callData.push(mainurl);
    });
    forkJoin(callData).subscribe(results => {
      this.processRespData(results);
    });
  }

  processRespData(response){
    response.forEach(item => {
      item['data'].forEach(element => {
        this.business.push(element);
      });
    });
  }

  

  onSubmit(value: any){
    console.log(value);
    let main_url = URL + '/api/user';
    let dob = moment(value['dob']).format("YYYY-MM-DD HH:mm:ss");
    let status = 0;
    if(value.employee){
      status = 0;
    } else {
      status = 1;
    }
    let req_data = {
      "user_id": this.userId,
      "primary_business": value.primary_business,
      "dob": dob,
      "address": value.address,
      "mobile": value.mobile,
      "email": value.email,
      "firstName": value.fname,
      "lastName": value.lname,
      "created_by": this.userId,
      "updated_by": this.userId,
      "is_used": 0,
      "name": value.name,
      "is_employee": status,
      "is_admin": 0
    }
    this.http.post(main_url, req_data).subscribe(res => {
      console.log(res);
      this.router.navigate(['/menu/users']);
      alert('successfully User Created');
    });
  }

}
