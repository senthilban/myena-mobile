import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  firstName: string;
  lastName: string;
  appName: string;
  pBusiness: any;
  addStatus;
  userId;
  business:any = {};
  award:any = {};
  boAward:any = {};
  socialLogin:any = {};
  myusers:any = [];

  constructor() {
  }

  setMyUsers(data){
    this.myusers = data;
  }

  getMyUsers(){
    return this.myusers;
  }

  setSocial(data){
    this.socialLogin = data;
  }

  getSocial(){
    return this.socialLogin;
  }

  setAppName(data){
    this.appName = data;
  }

  getAppName(){
    return this.appName;
  }

  setPrimaryBusiness(data){
    this.pBusiness = data;
  }

  getPrimaryBusiness(){
    return this.pBusiness;
  }

  setUserName(firstName, lastName) {
      this.firstName = firstName;
      this.lastName = lastName;       
  }

  getUserName() {
      return this.firstName + ' ' + this.lastName;
  }

  setUserId(id){
    this.userId = id;
  }

  getUserId(){
    return this.userId;
  }

  setBusiness(data){
    this.business = data;
  }

  getBusiness(){
    return this.business;
  }

  setAddStatus(status){
    this.addStatus = status;
  }

  getAddStatus(){
    return this.addStatus;
  }

  setAward(data){
    this.award =data;
  }

  getAward(){
    return this.award;
  }

  setBOAward(data){
    this.boAward = data;
  }

  getBOAward(){
    return this.boAward;
  }
}
